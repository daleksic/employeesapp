<?php

namespace Controller;

use Model\User;
use Model\UserRepository;

class UserController extends BaseController
{
    public function readAction()
    {
        $userId = isset($_GET['userId']) ? $_GET['userId'] : null;
        $userRepository = new UserRepository();
        if ($userId) {
            $vars['user'] = $userRepository->getById($userId);
        } else {
            $vars['users'] = $userRepository->getAll();
        }

        $this->render('user:read', $vars);
    }
}