<?php

namespace Controller;

class BaseController
{
    protected function render($view, $vars = null)
    {
        $viewExploded = explode(':', $view);

        require_once ('./View/' . $viewExploded[0] . '/' . $viewExploded[1] . '.view.php');
    }
}